/*
** fox_setpixel.c for Set Pixel in /home/mei/Perso/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Mon Dec 21 23:34:19 2015 Michel Mancier
** Last update Wed Jan 27 02:13:30 2016 Walter Bonetti
*/

#include "../include/foxtek.h"

void		fox_setpixel(t_bunny_pixelarray *pix,
			     t_bunny_position *pos,
			     t_color *color)
{
  if (pos->y < pix->clipable.buffer.height && pos->y >= 0 &&
      pos->x < pix->clipable.buffer.width && pos->x >= 0)
    ((t_color *)pix->pixels)[pos->y *
  pix->clipable.clip_width + pos->x] = *color;
}
