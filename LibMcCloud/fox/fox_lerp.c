/*
** fox_lerp.c for  in /home/bonett_w/Documents/perso/LibMcCloud
**
** Made by Walter Bonetti
** Login   <bonett_w@epitech.net>
**
** Started on  Sun Dec 27 22:17:47 2015 Walter Bonetti
** Last update Mon Dec 28 15:55:48 2015 Michel Mancier
*/

#include "foxtek.h"

float	fox_lerping(float v0, float v1, float t)
{
  return ((1 - t) * v1 + t * v0);
}

t_color		fox_lerp(unsigned int	to,
			 unsigned int	color,
			 float		amount)
{
  t_color	fout;
  t_color	fcolor;
  t_color	fto;

  fto.full = to;
  fcolor.full = color;
  fout.full = (RGBA_C((int)fox_lerping(fcolor.argb[0], fto.argb[0],
		       amount),
		      (int)fox_lerping(fcolor.argb[1], fto.argb[1],
		       amount),
		      (int)fox_lerping(fcolor.argb[2], fto.argb[2],
		       amount),
		      255));
  return (fout);
}
