/*
** fox_blit_downright.c for Fox Blit Downright in /home/mei/GitHub/LibMcCloud
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Sun Dec 27 22:16:15 2015 Michel Mancier
** Last update Mon Dec 28 15:59:01 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_downright(t_bunny_window      	*win,
					   t_bunny_pixelarray	*pix)
{
  t_bunny_position	pos;

  pos.x = win->buffer.width - pix->clipable.clip_width - 1;
  pos.y = win->buffer.height - pix->clipable.clip_height - 1;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
