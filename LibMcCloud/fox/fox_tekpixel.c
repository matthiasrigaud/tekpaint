/*
** fox_tekpixel.c for  in /home/bonett_w/Documents/perso/LibMcCloud/fox/
**
** Made by walter bonetti
** Login   <bonett_w@epitech.eu>
**
** Started on  Fri Jan  8 15:49:43 2016 walter bonetti
** Last update Tue Jan 26 00:00:25 2016 Walter Bonetti
*/

#include <lapin.h>

void	fox_tekpixel(t_bunny_pixelarray		*pix,
		     t_bunny_position		*pos,
		     unsigned int		color)
{
  if (pos->y < pix->clipable.buffer.height && pos->y >= 0 &&
      pos->x < pix->clipable.buffer.width && pos->x >= 0)
    ((unsigned int *)(pix->pixels))[(pos->y *
     pix->clipable.clip_width) + pos->x] = color;
}
