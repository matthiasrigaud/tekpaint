/*
** fox_fillarray.c for Fox Fill Array in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 12:59:35 2015 Michel Mancier
** Last update Wed Dec 23 16:27:59 2015 Michel Mancier
*/

#include "foxtek.h"

void	fox_fill_array(t_bunny_pixelarray *pix,
		      t_color *color)
{
  int	size;
  int	i;

  i = -1;
  size = pix->clipable.clip_height * pix->clipable.clip_width;
  while (i < size)
    ((unsigned int *)pix->pixels)[++i] = color->full;
}
