/*
** fox_draw_circle.c for Fox Draw Circle in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 16:29:22 2015 Michel Mancier
** Last update Fri Jan  8 15:55:11 2016 walter bonetti
*/

#include <math.h>
#include "foxtek.h"

void			fox_draw_circle(t_bunny_pixelarray	*pix,
					t_bunny_position	*center,
					unsigned int		radius,
					t_color			*color)
{
  double		i;
  t_bunny_position	pos;

  i = 0;
  while (i < 360)
    {
      pos.x = center->x + cos(i / 180.0f * M_PI) * radius;
      pos.y = center->y + sin(i / 180.0f * M_PI) * radius;
      fox_setpixel(pix, &pos, color);
      i += 0.1;
    }
}

void			fox_draw_circle_faster(t_bunny_pixelarray	*pix,
					       t_bunny_position	*center,
					       unsigned int		radius,
					       t_color			*color)
{
  int			i;
  t_bunny_position	pos;

  i = 0;
  while (i < 360)
    {
      pos.x = center->x + cos(i / 180.0f * M_PI) * radius;
      pos.y = center->y + sin(i / 180.0f * M_PI) * radius;
      fox_setpixel(pix, &pos, color);
      i++;
    }
}
