/*
** fox_load_effect.c for Fox Load Effect in /home/mei/GitHub/LibMcCloud/fox
**
** Made by Michel Mancier
** Login   <mei@epitech.net>
**
** Started on  Mon Dec 28 16:13:46 2015 Michel Mancier
** Last update Fri Feb  5 18:54:39 2016 Matthias RIGAUD
*/

#include "foxtek.h"

int	fox_load_effect(t_bunny_effect *effect, char *filepath, int volume)
{
  if ((effect = bunny_load_effect(filepath)) == NULL)
    return (ERR_NOLOAD_MUSIC);
  bunny_sound_volume(&effect->sound, volume);
  bunny_sound_loop(&effect->sound, 0);
  bunny_sound_play(&effect->sound);
  return (0);
}
