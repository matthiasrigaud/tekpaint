/*
** fox_calc_color.c for Fox Calc Alpha in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Sat Dec 26 16:33:41 2015 Michel Mancier
** Last update Sat Dec 26 16:39:18 2015 Michel Mancier
*/

#include "foxtek.h"

t_color	fox_calc_color(t_color *newc,
		       t_color *oldc)
{
  t_color	res;

  res.full = oldc->full * (1 - newc->argb[3] / 255)
      + newc->full * (newc->argb[3] / 255);
  return (res);
}
