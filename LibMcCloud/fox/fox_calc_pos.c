/*
** fox_calc_pos.c for Fox Calc Pos in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Sat Dec 26 13:30:05 2015 Michel Mancier
** Last update Thu Jan 28 14:36:26 2016 Matthias RIGAUD
*/

#include "foxtek.h"

int	fox_calc_pos(int x, int y, const t_bunny_pixelarray *pix)
{
  return (y * pix->clipable.clip_width + x);
}
