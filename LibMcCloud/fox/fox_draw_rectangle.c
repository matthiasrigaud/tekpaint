/*
** fox_draw_rectangle.c for Fox Draw Rectangle in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 16:34:04 2015 Michel Mancier
** Last update Tue Dec 29 21:18:57 2015 walter bonetti
*/

#include "foxtek.h"

void			fox_draw_rectangle(t_bunny_pixelarray	*pix,
					   t_bunny_position	pos,
					   t_bunny_position	size,
					   t_color		color)
{
  t_bunny_position	axis;

  axis.y = pos.y;
  while (axis.y < (size.y + pos.y))
    {
      axis.x = pos.x;
      while (axis.x < (pos.x + size.x))
	{
	  if (axis.x == pos.x ||
	      axis.x == (pos.x + size.x - 1) ||
	      axis.y == pos.y || axis.y == (pos.y + size.y - 1))
	    fox_setpixel(pix, &axis, &color);
	  axis.x++;
	}
      axis.y++;
    }
}

void			fox_draw_rectangle_fill(t_bunny_pixelarray	*pix,
						t_bunny_position	pos,
						t_bunny_position	size,
						t_color			color)
{
  int			i;
  t_bunny_position	draw;

  i = 0;
  while (i < size.x * size.y)
    {
      draw.x = pos.x + i % size.x;
      draw.y = pos.y + i / size.x;
      fox_setpixel(pix, &draw, &color);
      ++i;
    }
}
