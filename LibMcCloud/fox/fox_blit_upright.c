/*
** fox_blit_upright.c for Fox Blit Upright in /home/mei/GitHub/LibMcCloud
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Sun Dec 27 22:16:15 2015 Michel Mancier
** Last update Mon Dec 28 15:47:02 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_upright(t_bunny_window		*win,
					t_bunny_pixelarray	*pix)
{
  t_bunny_position	pos;

  pos.x = win->buffer.width - pix->clipable.clip_width - 1;
  pos.y = 0;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
