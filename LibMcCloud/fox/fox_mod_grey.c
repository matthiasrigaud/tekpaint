/*
** fox_modgrey.c for Fox Mod Grey in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 13:07:34 2015 Michel Mancier
** Last update Sat Jan 30 22:52:34 2016 Matthias RIGAUD
*/

#include "foxtek.h"

void		fox_mod_grey(t_bunny_pixelarray *pix)
{
  t_color		c;
  t_bunny_position	pos;
  int			i;

  pos.y = 0;
  while (pos.y < pix->clipable.clip_height)
    {
      pos.x = 0;
      while (pos.x < pix->clipable.clip_width)
	{
	  c = fox_getpixel(pix, &pos);
	  c.argb[0] = RGBA_G(c.argb[2], c.argb[1], c.argb[0], c.argb[3]);
	  c.full = RGBA_C(c.argb[0], c.argb[0], c.argb[0], c.argb[3]);
	  i = pos.y * pix->clipable.clip_width + pos.x;
	  ((unsigned int *)pix->pixels)[i] = c.full;
	  ++pos.x;
	}
      ++pos.y;
    }
}
