/*
** init.c for  in /home/bonett_w/Documents/perso/LibMcCloud/fox/window/
**
** Made by walter bonetti
** Login   <bonett_w@epitech.eu>
**
** Started on  Wed Jan  6 12:11:57 2016 walter bonetti
** Last update Wed Jan 27 01:15:36 2016 Walter Bonetti
*/

#include "../../include/foxwindow.h"

void	fox_window_init_screen(t_fox_window *window, unsigned int color)
{
  window->buffer = bunny_new_pixelarray(window->win->buffer.width,
					window->win->buffer.height);
  fox_window_color(window, color);
}

void	fox_window_color(t_fox_window *window, unsigned int color)
{
  int	i;

  i = 0;
  while (i < window->buffer->clipable.clip_height
	 * window->buffer->clipable.clip_width)
  ((unsigned int *)window->buffer->pixels)[++i] = color;
}
void	fox_window_set_screen(t_fox_window 		*window,
			      t_bunny_pixelarray 	*pix)
{
  window->screen = pix;
}

t_fox_window	*fox_window_default_darkfox(unsigned int w,
					    unsigned int h,
					    t_bunny_window_style winstyle,
					    char *name)
{
  t_fox_window	*win;
  win = fox_window_new(w, h, winstyle, name);
  fox_window_init_screen(win, BLACK);
  return (win);
}
