/*
** setter.c for  in /home/bonett_w/Documents/perso/LibMcCloud/fox/window/
**
** Made by walter bonetti
** Login   <bonett_w@epitech.eu>
**
** Started on  Tue Dec 29 23:48:41 2015 walter bonetti
** Last update Mon Jan 25 19:32:36 2016 Walter Bonetti
*/

#include <stdlib.h>
#include "../../include/foxwindow.h"
#include "../../include/my.h"

void		fox_window_set_loop(t_fox_window 	*window,
				    t_bunny_loop 	loop)
{
  window->loop = loop;
}

void		fox_window_set_key(t_fox_window 	*window,
				   t_bunny_key 		key)
{
  window->key = key;
}

void		fox_window_set_mouse(t_fox_window 		*window,
				     t_bunny_click 		click,
				     t_bunny_move 		move)
{
  window->click = click;
  window->move = move;
  window->mouse.state = 1;
}

void		fox_window_starter(t_fox_window *window)
{
  if (window->loop == NULL)
    {
      my_printf(1, "[Fox] no loop set !\n");
      return ;
    }
  bunny_set_loop_main_function(window->loop);
  if (window->key != NULL)
    {
      my_printf(1, "[Fox] Listening keyboard event...\n");
      bunny_set_key_response(window->key);
    }
  if (window->move != NULL && window->click != NULL)
    {
      my_printf(1, "[Fox] Listening mouse event...\n");
      bunny_set_click_response(window->click);
      bunny_set_move_response(window->move);
    }
  bunny_loop(window->win, 60, window);
}
