/*
** window.c for  in /home/bonett_w/Documents/perso/LibMcCloud/fox/window/
**
** Made by walter bonetti
** Login   <bonett_w@epitech.eu>
**
** Started on  Tue Dec 29 23:24:52 2015 walter bonetti
** Last update Wed Jan 27 01:12:42 2016 Walter Bonetti
*/

#include <stdlib.h>
#include <lapin.h>
#include "../../include/my.h"
#include "../../include/foxwindow.h"

t_fox_window	*fox_window_new(unsigned int 		width,
				unsigned int 		height,
				t_bunny_window_style 	winstyle,
				const char 		*window_name)
{
  t_fox_window	*window;

  window = bunny_malloc(sizeof(t_fox_window));
  if (window == NULL)
    {
      my_printf(2, "Error: Malloc: t_fox_window\n");
      return (NULL);
    }
  window->win = bunny_start_style(width, height, winstyle, window_name);
  window->zero_axis.x = 0;
  window->zero_axis.y = 0;
  window->click = NULL;
  window->move = NULL;
  window->key = NULL;
  window->loop = NULL;
  window->screen = NULL;
  window->buffer = NULL;
  window->mouse.pos = bunny_get_mouse_position();
  return (window);
}

void		fox_window_delete(t_fox_window *window)
{
  bunny_stop(window->win);
  bunny_delete_clipable(&window->screen->clipable);
  bunny_delete_clipable(&window->buffer->clipable);
}
