/*
** fox_setline.c for Set Line in /home/mei/Perso/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Mon Dec 21 23:37:32 2015 Michel Mancier
** Last update Fri Jan  8 16:10:24 2016 walter bonetti
*/

#include "foxtek.h"
#include "my.h"

static int	set_tab_drawline(t_bunny_position	c[4],
				 t_bunny_position 	p1,
				 t_bunny_position 	p2)
{
  c[0].x = p2.x - p1.x;
  c[0].y = p2.y - p1.y;
  c[1].x = SGN(c[0].x);
  c[1].y = SGN(c[0].y);
  c[0].x = ABS(c[0].x);
  c[0].y = ABS(c[0].y);
  c[2].x = c[0].y >> 1;
  c[2].y = c[0].x >> 1;
  c[3].x = p1.x;
  c[3].y = p1.y;
  return (0);
}

static void	draw_line_octan(t_bunny_pixelarray	*pix,
				t_bunny_position	c[4],
				unsigned int       	color)
{
  int		i;

  i = 0;
  while (i < c[0].x)
    {
      c[2].y = c[2].y + c[0].y;
      if (c[2].y >= c[0].x)
	{
	  c[2].y = c[2].y - c[0].x;
	  c[3].y = c[3].y + c[1].y;
	}
      c[3].x = c[3].x + c[1].x;
      fox_tekpixel(pix, &c[3], color);
      i = i + 1;
    }
}

static void	draw_line_octan_inf(t_bunny_pixelarray	*pix,
				    t_bunny_position	c[4],
				    unsigned int	color)
{
  int		i;

  i = 0;
  while (i < c[0].y)
    {
      c[2].x = c[2].x + c[0].x;
      if (c[2].x >= c[0].y)
	{
	  c[2].x = c[2].x - c[0].y;
	  c[3].x = c[3].x + c[1].x;
	}
      c[3].y = c[3].y + c[1].y;
      fox_tekpixel(pix, &c[3], color);
      i = i + 1;
    }
}
void	fox_draw_line(t_bunny_pixelarray	*pix,
		      t_bunny_position		*pos,
		      unsigned int		color)
{
  t_bunny_position	c[4];

  set_tab_drawline(c, pos[0], pos[1]);
  fox_tekpixel(pix, &pos[0], color);
  if (c[0].x >= c[0].y)
    draw_line_octan(pix, c, color);
  else
    draw_line_octan_inf(pix, c, color);
}
