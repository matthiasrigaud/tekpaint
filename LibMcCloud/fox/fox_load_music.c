/*
** fox_load_music.c for Fox Load Music in /home/mei/GitHub/LibMcCloud/fox
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Mon Dec 28 16:03:24 2015 Michel Mancier
** Last update Fri Feb  5 18:53:54 2016 Matthias RIGAUD
*/

#include "foxtek.h"

int	fox_load_music(t_bunny_music *music, char *filepath, int volume)
{
  if ((music = bunny_load_music(filepath)) == NULL)
    return (ERR_NOLOAD_MUSIC);
  bunny_sound_volume(&music->sound, volume);
  bunny_sound_loop(&music->sound, 1);
  bunny_sound_play(&music->sound);
  return (0);
}
