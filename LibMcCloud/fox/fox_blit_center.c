/*
** fox_blit_center.c for Fox Blit Center in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 18:16:25 2015 Michel Mancier
** Last update Wed Dec 23 18:24:31 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_center(t_bunny_window		*win,
					t_bunny_pixelarray	*pix)
{
  t_bunny_position	pos;

  pos.x = (win->buffer.width - pix->clipable.clip_width) / 2;
  pos.y = (win->buffer.height - pix->clipable.clip_height) / 2;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
