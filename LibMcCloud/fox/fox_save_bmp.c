/*
** fox_save_bmp.c for Fox BmpSaver in /home/zimmer_n
**
** Made by Nicolas Zimmermann
** Login   <zimmer_n@epitech.net>
**
** Started on  Mon Jan 25 20:32:03 2016 Nicolas Zimmermann
** Last update Sat Jan 30 23:00:44 2016 Matthias RIGAUD
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "foxtek.h"
#include "my.h"

void	fox_writebmpheader(const int fd, t_fox_bmpheader *header,
			   const t_bunny_pixelarray *pix)
{
  header->fileheader.filetype[0] = 'B';
  header->fileheader.filetype[1] = 'M';
  header->fileheader.filesize = sizeof(t_fox_bmpheader) + HB * WB * 3;
  header->fileheader.reserved[0] = 42;
  header->fileheader.reserved[1] = 42;
  header->fileheader.dataoffset = sizeof(t_fox_bmpheader);
  header->headersize = sizeof(t_fox_bmpheader) - sizeof(t_fox_headerfile);
  header->width = WB;
  header->height = HB;
  header->planes = 1;
  header->bitsperpixels = 24;
  header->compression = 0;
  header->bitmapsize = WB * HB * 3;
  header->horizontalres = WB;
  header->verticalres = HB;
  header->numcolors = 0;
  header->impcolors = 0;
  write(fd, header, sizeof(t_fox_bmpheader));
}

void			fox_pixeltobmp(const int fd, const t_bunny_pixelarray *img)
{
  t_color		*pix;
  t_bunny_position	pos;

  pix = img->pixels;
  pos.y = img->clipable.buffer.height;
  while (--pos.y >= 0)
    {
      pos.x = -1;
      while (++pos.x < img->clipable.buffer.width)
	{
	  write(fd, &pix[getxy(pos.x, pos.y, img)].argb[2], 1);
	  write(fd, &pix[getxy(pos.x, pos.y, img)].argb[1], 1);
	  write(fd, &pix[getxy(pos.x, pos.y, img)].argb[0], 1);
	}
    }
}

void			fox_save_bmp(const char *path, const t_bunny_pixelarray *img)
{
  int			fd;
  t_fox_bmpheader	header;

  fd = open(path, O_WRONLY | O_CREAT, S_IROTH | S_IRGRP | S_IRUSR | S_IWUSR);
  fox_writebmpheader(fd, &header, img);
  fox_pixeltobmp(fd, img);
  close(fd);
}
