/*
** fox_getpixel.c for Fox Get Pixel in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 13:01:57 2015 Michel Mancier
** Last update Wed Jan 27 02:13:07 2016 Walter Bonetti
*/

#include "foxtek.h"

t_color	fox_getpixel(t_bunny_pixelarray *pix,
		     t_bunny_position *pos)
{
  return (((t_color *)pix->pixels)[pos->y *
	  pix->clipable.clip_width + pos->x]);
}

unsigned int	fox_getpixelxy(t_bunny_pixelarray *pix,
			     int		x,
			     int		y)
{
  return (((unsigned int *)pix->pixels)[y * pix->clipable.clip_width + x]);
}
