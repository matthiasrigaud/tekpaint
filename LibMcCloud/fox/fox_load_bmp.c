/*
** fox_bmploader.c for Fox BmpLoader in /home/mei/Perso/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Tue Dec 22 15:03:11 2015 Michel Mancier
** Last update Fri Jan 29 11:09:34 2016 Walter Bonetti
*/

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include "foxtek.h"
#include "my.h"

t_bunny_pixelarray	*fox_load_bmp(const char *path)
{
  t_bunny_pixelarray	*img;
  t_fox_bmpheader	bitmap;
  int			fd;

  fd = open(path, O_RDONLY);
  bitmap = fox_getbmpheader(fd);
  my_printf(1, "[Image][BMP] Loading...\n");
  if (bitmap.fileheader.filetype[0] == 'B' &&
      bitmap.fileheader.filetype[1] == 'M')
    {
      img = bunny_new_pixelarray(bitmap.width, bitmap.height);
      if (img != NULL)
	my_printf(1, "Loading: %s%sOk !%s\n", FOREGREEN, STYLEBOLD, STYLERESET);
      fox_bmptopixel(fd, img, &bitmap);
    }
  else
    my_printf(1, "Loading: %s%sError !%s\n", FORERED, STYLEBOLD, STYLERESET);
  return (img);
}

void		fox_bmptopixel(int			fd,
			       t_bunny_pixelarray	*pix,
			       t_fox_bmpheader		*bitmap)
{
  t_color		color;
  t_bunny_position	pos;

  pos.y = pix->clipable.clip_height - 1;
  while (pos.y >= 0)
    {
      pos.x = 0;
      while (pos.x < pix->clipable.clip_width)
	{
	  read(fd, &color.argb[2], sizeof(unsigned char));
	  read(fd, &color.argb[1], sizeof(unsigned char));
	  read(fd, &color.argb[0], sizeof(unsigned char));
	  if (bitmap->bitsperpixels == 32)
	    read(fd, &color.argb[3], sizeof(unsigned char));
	  else
	    color.argb[3] = 255;
	  fox_setpixel(pix, &pos, &color);
	  ++pos.x;
	}
      --pos.y;
    }
  fox_dispbmpheader(bitmap);
}

void	fox_dispbmpheader(t_fox_bmpheader *b)
{
  my_printf(1, "Filetype: %c %c\n",
	    b->fileheader.filetype[0],
	    b->fileheader.filetype[1]);
  my_printf(1, "Filesize: %u\n", b->fileheader.filesize);
  my_printf(1, "Reserved: %d %d\n",
	    b->fileheader.reserved[0],
	    b->fileheader.reserved[1]);
  my_printf(1, "Dataoffset: %u\n", b->fileheader.dataoffset);
  my_printf(1, "Headersize: %u\n", b->headersize);
  my_printf(1, "Width: %d\n", b->width);
  my_printf(1, "Height: %d\n", b->height);
  my_printf(1, "Planes: %d\n", b->planes);
  my_printf(1, "Bitsperpixels: %d\n", b->bitsperpixels);
  my_printf(1, "Compression: %u\n", b->compression);
  my_printf(1, "Bitmapsize: %u\n", b->bitmapsize);
  my_printf(1, "Horizontalres: %d\n", b->horizontalres);
  my_printf(1, "Vertivalres: %d\n", b->verticalres);
  my_printf(1, "NumColors in palette: %u\n", b->numcolors);
  my_printf(1, "Importantcolos: %u\n", b->impcolors);
}

t_fox_bmpheader		fox_getbmpheader(int fd)
{
  t_fox_bmpheader	header;

  my_printf(1, "[Image][Bmp] Reading Header...\n");
  read(fd, &header, sizeof(t_fox_bmpheader));
  my_printf(1, "Sizeof read : %u\n", sizeof(t_fox_bmpheader));
  return (header);
}
