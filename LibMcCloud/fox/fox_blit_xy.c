/*
** fox_blit_xy.c for Fox Blit XY in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 18:16:25 2015 Michel Mancier
** Last update Mon Dec 28 15:57:26 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_xy(t_bunny_window		*win,
				    t_bunny_pixelarray	*pix,
				    int			x,
				    int			y)
{
  t_bunny_position	pos;

  pos.x = x;
  pos.y = y;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
