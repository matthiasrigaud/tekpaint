/*
** fox_new_pixelarray.c for Fox New Pixelarray in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 17:24:46 2015 Michel Mancier
** Last update Wed Dec 23 17:30:39 2015 Michel Mancier
*/

#include "foxtek.h"

t_bunny_pixelarray	*fox_new_pixelarray(unsigned int	width,
					    unsigned int	height,
					    unsigned int	color)
{
  t_bunny_pixelarray	*pix;
  int			size;
  int			i;

  pix = bunny_new_pixelarray(width, height);
  i = -1;
  size = height * width;
  while (i < size)
    ((unsigned int *)pix->pixels)[++i] = color;
  return (pix);
}
