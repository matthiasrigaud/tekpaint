/*
** fox_draw_polygon.c for Fox Draw Forme in /home/mei/GitHub/LibMcCloud/
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.eu>
**
** Started on  Wed Dec 23 17:09:51 2015 Michel Mancier
** Last update Tue Jan 19 13:57:05 2016 Walter Bonetti
*/

#include "../include/foxtek.h"

void			fox_draw_polygon(t_bunny_pixelarray	*pix,
					 t_bunny_position	*pos,
					 unsigned int		nb,
					 t_color		*color)
{
  unsigned int		i;
  t_bunny_position	posi[2];

  i = 1;
  while (i < nb)
    {
      posi[0] = pos[i - 1];
      posi[1] = pos[i];
      fox_draw_line(pix, posi, color->full);
      ++i;
    }
    posi[0] = pos[i - 1];
    posi[1] = pos[0];
    fox_draw_line(pix, posi, color->full);
}
