/*
** fox_blit_upleft.c for Fox Blit Upleft in /home/mei/GitHub/LibMcCloud
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Sun Dec 27 22:16:15 2015 Michel Mancier
** Last update Sun Dec 27 22:17:26 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_upleft(t_bunny_window		*win,
					t_bunny_pixelarray	*pix)
{
  t_bunny_position	pos;

  pos.x = 0;
  pos.y = 0;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
