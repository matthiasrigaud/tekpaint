/*
** fox_blit_downleft.c for Fox Blit Downleft in /home/mei/GitHub/LibMcCloud
**
** Made by Michel Mancier
** Login   <mancie_m@epitech.net>
**
** Started on  Sun Dec 27 22:16:15 2015 Michel Mancier
** Last update Mon Dec 28 15:58:08 2015 Michel Mancier
*/

#include "foxtek.h"

void			fox_blit_downleft(t_bunny_window       	*win,
					  t_bunny_pixelarray	*pix)
{
  t_bunny_position	pos;

  pos.x = 0;
  pos.y = win->buffer.height - pix->clipable.clip_height - 1;
  bunny_blit(&win->buffer, &pix->clipable, &pos);
}
